<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FirstController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function number()
    {
        return $this->render('pages/home.html.twig', [
           
            ]);
    }
}
